terminal package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   terminal.application

Submodules
----------

terminal.app module
-------------------

.. automodule:: terminal.app
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: terminal
   :members:
   :undoc-members:
   :show-inheritance:
