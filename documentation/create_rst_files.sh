#!/usr/bin/env bash

if ! [[ "$PWD" =~ .+?(\/TwitchBot\/documentation) ]]; then
    echo "You're in the wrong directory. Please change to /TwitchBot/documentation/"
    echo "Exiting ..."
    exit 1
fi

sphinx-apidoc -o source ../

printf "\nINFO: Run 'make html' (from this directory) to build the documentation.\n"
