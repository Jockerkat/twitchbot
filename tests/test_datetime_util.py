# -*- coding: utf-8 -*-
"""This file contains tests related to the datetime_util file."""

from datetime import datetime
from util import datetime_util


def test_posix_timestamp_to_datetime():
    time_format: str = '%Y-%m-%d %H:%M:%S.%f'
    timezone: str = 'Europe/Zurich'

    # First value: POSIX timestamp; second value: expected datetime
    tests_utc: tuple = ((1624288239674, '2021-06-21 15:10:39.674000'),
                        (1642695244292, '2022-01-20 16:14:04.292000'))

    tests_localised: tuple = ((1624288239674, '2021-06-21 17:10:39.674000'),
                              (1642695244292, '2022-01-20 17:14:04.292000'))

    for test in tests_utc:
        assert datetime_util.posix_timestamp_to_datetime(test[0]) == datetime.strptime(test[1], time_format)

    for test in tests_localised:
        assert datetime_util.posix_timestamp_to_datetime(test[0], timezone) == datetime.strptime(test[1], time_format)
