# -*- coding: utf-8 -*-
"""This file contains tests related to the terminal_util file."""

from util import terminal_util


def test_terminal_util_header():
    text: str = f"Lorem ipsum {terminal_util.TerminalColours.HEADER}dolor{terminal_util.TerminalColours.END} sit amet."
    correct_output: str = "Lorem ipsum \033[95mdolor\033[0m sit amet."

    assert text == correct_output


def test_terminal_util_ok():
    text: str = f"Lorem ipsum {terminal_util.TerminalColours.OK}dolor{terminal_util.TerminalColours.END} sit amet."
    correct_output: str = "Lorem ipsum \033[92mdolor\033[0m sit amet."

    assert text == correct_output


def test_terminal_util_warning():
    text: str = f"Lorem ipsum {terminal_util.TerminalColours.WARNING}dolor{terminal_util.TerminalColours.END} sit amet."
    correct_output: str = "Lorem ipsum \033[93mdolor\033[0m sit amet."

    assert text == correct_output


def test_terminal_util_fail():
    text: str = f"Lorem ipsum {terminal_util.TerminalColours.FAIL}dolor{terminal_util.TerminalColours.END} sit amet."
    correct_output: str = "Lorem ipsum \033[91mdolor\033[0m sit amet."

    assert text == correct_output


def test_terminal_util_bold():
    text: str = f"Lorem ipsum {terminal_util.TerminalColours.BOLD}dolor{terminal_util.TerminalColours.END} sit amet."
    correct_output: str = "Lorem ipsum \033[1mdolor\033[0m sit amet."

    assert text == correct_output
