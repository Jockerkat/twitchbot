# -*- coding: utf-8 -*-
"""This file contains tests related to the list_util file."""

from util import list_util

LIST_OF_DICTIONARIES = [
    {
        "position": 3,
        "dict_3_key_2": "dict_3_value_2"
    },
    {
        "position": 1,
        "dict_1_key_2": "dict_1_value_2"
    },
    {
        "position": 2,
        "dict_2_key_2": "dict_2_value_2"
    }
]


def test_sort_list_of_dictionaries_by_value():
    expected_data: list = [
        {
            "position": 1,
            "dict_1_key_2": "dict_1_value_2"
        },
        {
            "position": 2,
            "dict_2_key_2": "dict_2_value_2"
        },
        {
            "position": 3,
            "dict_3_key_2": "dict_3_value_2"
        }
    ]

    assert expected_data == list_util.sort_list_of_dictionaries_by_value(LIST_OF_DICTIONARIES, 'position')


def test_get_values_from_list_of_dictionaries():
    expected_data: list = [3, 1, 2]

    assert expected_data == list_util.get_values_from_list_of_dictionaries(LIST_OF_DICTIONARIES, 'position')
