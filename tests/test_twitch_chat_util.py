# -*- coding: utf-8 -*-
"""This file contains tests related to the twitch_chat_util file."""

import irc.client

from util import twitch_chat_util


def test_get_event_tags():
    event: irc.client.Event = irc.client.Event(type="pubmsg",
                                               source="jockerkat!jockerkat@jockerkat.tmi.twitch.tv",
                                               target="#jockerkat",
                                               arguments=['test'],
                                               tags=[{'key': 'badge-info', 'value': None},
                                                     {'key': 'badges', 'value': 'broadcaster/1'},
                                                     {'key': 'client-nonce',
                                                      'value': 'a24c5f7d27435b4fe0a348436ab88fe8'},
                                                     {'key': 'color', 'value': '#FF0000'},
                                                     {'key': 'display-name', 'value': 'Jockerkat'},
                                                     {'key': 'emotes', 'value': None},
                                                     {'key': 'first-msg', 'value': '0'},
                                                     {'key': 'flags', 'value': None},
                                                     {'key': 'id', 'value': '7d92af03-d003-48c4-8380-d920a63dae16'},
                                                     {'key': 'mod', 'value': '0'},
                                                     {'key': 'room-id', 'value': '112611272'},
                                                     {'key': 'subscriber', 'value': '0'},
                                                     {'key': 'tmi-sent-ts', 'value': '1642935030479'},
                                                     {'key': 'turbo', 'value': '0'},
                                                     {'key': 'user-id', 'value': '112611272'},
                                                     {'key': 'user-type', 'value': None}])

    tags: dict = {'badge-info': None,
                  'badges': 'broadcaster/1',
                  'client-nonce': 'a24c5f7d27435b4fe0a348436ab88fe8',
                  'color': '#FF0000',
                  'display-name': 'Jockerkat',
                  'emotes': None,
                  'first-msg': '0', 'flags': None,
                  'id': '7d92af03-d003-48c4-8380-d920a63dae16',
                  'mod': '0',
                  'room-id': '112611272',
                  'subscriber': '0',
                  'tmi-sent-ts': '1642935030479',
                  'turbo': '0',
                  'user-id': '112611272',
                  'user-type': None}

    assert twitch_chat_util.get_event_tags(event) == tags


def test_strip_unicode_characters():
    test_string: str = "this is a random tex͝t; do you know the wåe; öäüéàè"
    correct_output: str = "this is a random text; do you know the wåe; öäüéàè"

    assert twitch_chat_util.strip_unicode_characters(test_string) == correct_output


def test_chat_message_is_spam():
    spam_list: list = ['\\b(a spam message)\\b']

    assert twitch_chat_util.chat_message_is_spam("a spam message", spam_list)
    assert twitch_chat_util.chat_message_is_spam("also a spam message", spam_list)
    assert not twitch_chat_util.chat_message_is_spam("not spam", spam_list)
