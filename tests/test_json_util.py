# -*- coding: utf-8 -*-
"""This file contains tests related to the json_util file."""

from util import json_util

TEST_CONFIG_PATH = './tests/resources/test_config.json'
TEST_CONFIG_CONTENTS: dict = {
    "key_1": "value_1",
    "key_2": "value_2",
    "key_3": [
        "array_value_1",
        "array_value_2",
        "array_value_3",
        "array_value_4",
        "array_value_5",
        "array_value_6",
        "array_value_7"
    ],
    "key_4": "value_4",
    "key_5": [
        {
            "position": 3,
            "dict_3_key_2": "dict_3_value_2"
        },
        {
            "position": 1,
            "dict_1_key_2": "dict_1_value_2"
        },
        {
            "position": 2,
            "dict_2_key_2": "dict_2_value_2"
        }
    ]
}


def __reset_test_config():
    with open(TEST_CONFIG_PATH, 'r+') as json_file:
        json_util.__write_to_file(json_file, TEST_CONFIG_CONTENTS)


def test_read_file():
    __reset_test_config()
    assert TEST_CONFIG_CONTENTS == json_util.read_file(TEST_CONFIG_PATH)
    __reset_test_config()


def test_update_value():
    __reset_test_config()

    json_util.update_value(TEST_CONFIG_PATH, 'key_2', 'new_value_2')

    expected_data: dict = {
        "key_1": "value_1",
        "key_2": "new_value_2",
        "key_3": [
            "array_value_1",
            "array_value_2",
            "array_value_3",
            "array_value_4",
            "array_value_5",
            "array_value_6",
            "array_value_7"
        ],
        "key_4": "value_4",
        "key_5": [
            {
                "position": 3,
                "dict_3_key_2": "dict_3_value_2"
            },
            {
                "position": 1,
                "dict_1_key_2": "dict_1_value_2"
            },
            {
                "position": 2,
                "dict_2_key_2": "dict_2_value_2"
            }
        ]
    }

    assert expected_data == json_util.read_file(TEST_CONFIG_PATH)
    __reset_test_config()


def test_update_value_in_array():
    __reset_test_config()

    json_util.update_value_in_array(TEST_CONFIG_PATH, 'key_3', 5, 'new_array_value_6')

    expected_data: dict = {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": [
            "array_value_1",
            "array_value_2",
            "array_value_3",
            "array_value_4",
            "array_value_5",
            "new_array_value_6",
            "array_value_7"
        ],
        "key_4": "value_4",
        "key_5": [
            {
                "position": 3,
                "dict_3_key_2": "dict_3_value_2"
            },
            {
                "position": 1,
                "dict_1_key_2": "dict_1_value_2"
            },
            {
                "position": 2,
                "dict_2_key_2": "dict_2_value_2"
            }
        ]
    }

    assert expected_data == json_util.read_file(TEST_CONFIG_PATH)
    __reset_test_config()


def test_append_value_to_array():
    __reset_test_config()

    json_util.append_value_to_array(TEST_CONFIG_PATH, 'key_3', 'array_value_8')

    expected_data: dict = {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": [
            "array_value_1",
            "array_value_2",
            "array_value_3",
            "array_value_4",
            "array_value_5",
            "array_value_6",
            "array_value_7",
            "array_value_8"
        ],
        "key_4": "value_4",
        "key_5": [
            {
                "position": 3,
                "dict_3_key_2": "dict_3_value_2"
            },
            {
                "position": 1,
                "dict_1_key_2": "dict_1_value_2"
            },
            {
                "position": 2,
                "dict_2_key_2": "dict_2_value_2"
            }
        ]
    }

    assert expected_data == json_util.read_file(TEST_CONFIG_PATH)
    __reset_test_config()


def test_delete_value_in_array():
    __reset_test_config()

    json_util.delete_value_in_array(TEST_CONFIG_PATH, 'key_3', 6)

    expected_data: dict = {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": [
            "array_value_1",
            "array_value_2",
            "array_value_3",
            "array_value_4",
            "array_value_5",
            "array_value_6"
        ],
        "key_4": "value_4",
        "key_5": [
            {
                "position": 3,
                "dict_3_key_2": "dict_3_value_2"
            },
            {
                "position": 1,
                "dict_1_key_2": "dict_1_value_2"
            },
            {
                "position": 2,
                "dict_2_key_2": "dict_2_value_2"
            }
        ]
    }

    assert expected_data == json_util.read_file(TEST_CONFIG_PATH)
    __reset_test_config()
