[![made-with-python](https://img.shields.io/badge/Made%20with-Python-blue.svg)](https://www.python.org/)
[![python-version](https://img.shields.io/badge/Python%20version-3.8-blue.svg)](https://www.python.org/)
[![license](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/Jockerkat/twitchbot/-/blob/master/LICENSE.md)

# TwitchBot

## About

TwitchBot is an IRC bot meant to connect to any Twitch.tv channel. Its primary purpose is to combat spam, i.e. read
incoming chat messages and determine, using the `SPAM_LIST` array in `config.json`, which supports regular
expressions, whether a message should be considered spam or not.

However, it can do more than that. Acknowledging subscriptions, re-subscriptions or gifted subscriptions from viewers is
very important to keep the viewer(s) invested in the channel. This is why TwitchBot can detect such events and thank the
person directly. Additionally, to improve the relationship between the channel's broadcaster and another broadcaster
raiding the former's channel, TwitchBot can react to such an event, thank the raider and give a shout-out to them.

TwitchBot has been confirmed to be working while being connected to a Twitch.tv channel with ~70k live viewers and
~500 messages per minute.

There are two versions of TwitchBot: *terminal* and *webapp*. The terminal version runs, evidently, in a terminal, while
the webapp version provides a nice web interface.

## Terminal Version

Click [here](terminal/README.md) for screenshots and how to get the terminal version of TwitchBot up and running.

## Webapp Version

Click [here](webapp/README.md) for screenshots and how to get the webapp version of TwitchBot up and running.

## Known Issues

This applies to both the terminal and webapp version of TwitchBot: Sometimes, the IRC bot is stuck on
`'Connecting to {server} on port {port} ...'`. In this case, restart the script (for the terminal version) or use the
stop and start buttons on the Twitch Bot page (for the webapp version).

## Changelog

[Changelog](CHANGELOG.md)

## License

[License](LICENSE.md)

## Credit

Credit for the icon goes to [Nociconist](https://thenounproject.com/search/?q=chatbot&i=2024259) whose illustration I
~~blatantly stole~~ got inspired from. Credit goes to Todd Birchard's
[blogpost](https://hackersandslackers.com/flask-application-factory/) and Kundan Singh's
[repository](https://github.com/ksh7/flask-starter) as well which has helped me in getting the webapp version of
TwitchBot up and running.