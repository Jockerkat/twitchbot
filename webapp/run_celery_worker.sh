#!/usr/bin/env bash

# Add root (/TwitchBot/) to PYTHONPATH
if [[ -z $PYTHONPATH ]]; then
  PYTHONPATH=$PYTHONPATH:$(pwd)
  export PYTHONPATH
fi

source venv/bin/activate

# Celery will only work if called from /TwitchBot/webapp/
if ! [[ "$PWD" =~ .+?(\/webapp) ]]; then
  cd "./webapp" || exit 1
fi

celery -A wsgi.celery worker -l INFO
