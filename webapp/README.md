# TwitchBot - Webapp Version

## Table of Contents

- [Screenshots](#screenshots)
    - [Home Page](#home-page)
    - [Twitch Bot Page: Clean](#twitch-bot-page-clean)
    - [Twitch Bot Page: IRC bot start](#twitch-bot-page-irc-bot-start)
    - [Twitch Bot Page: Messages received](#twitch-bot-page-messages-received)
    - [Twitch Bot Page: IRC bot stop](#twitch-bot-page-irc-bot-stop)
    - [Twitch Settings Page](#twitch-settings-page)
- [Quick Setup](#quick-setup)
- [Configuration](#configuration)
- [Known Issues](#known-issues)

## Screenshots

### Home Page

![](screenshots/home_page.png)

### Twitch Bot Page: Clean

![](screenshots/twitch_bot_page_clean.png)

### Twitch Bot Page: IRC bot start

![](screenshots/twitch_bot_page_started.png)

### Twitch Bot Page: Messages received

![](screenshots/twitch_bot_page_messages_received.png)
![](screenshots/twitch-tv_chat_messages.png)

### Twitch Bot Page: IRC bot stop

![](screenshots/twitch_bot_page_stopped.png)

### Twitch Settings Page

![](screenshots/settings_page.png)

## Quick Setup

For all steps below (except step 1), it is assumed that you are in the root directory (`/TwitchBot/`).

1. Download the latest release from [here](https://gitlab.com/Jockerkat/twitchbot/-/releases).
2. Create a python virtual environment with the following command: `python3 -m venv venv`.
3. Install the python dependencies with the following command: `pip3 install -r requirements_webapp.txt`.
4. Download the stable build of Redis from [here](https://redis.io/download) and install it to `./webapp/`.
   Make sure that the redis version in `./webapp/run_redis_server.sh` (line 4) matches the version you downloaded.
5. Get your Twitch.tv *Client ID* by creating a new application on
   Twitch.tv's [developer page](https://dev.twitch.tv/console/apps). Once created, press *Manage* on the right to get
   the ID.
6. Get your Twitch.tv *Client Secret* by pressing *New Secret* at the bottom of the page from the previous step.
7. Get your Twitch.tv IRC *Token* by going to [this website](https://twitchapps.com/tmi). Click on *Connect*, then copy
   the token sans "oauth:".
8. Create a `.env` file in the `./webapp/application/` directory and add the following to it:
    ```plaintext
    CLIENT_ID = your_client_id_here
    CLIENT_SECRET = your_client_secret_here
    TOKEN = your_token_here
    ```
9. Make any modifications to the `config.json` file to suit your needs.
10. Open three terminals and run the following bash scripts: `./webapp/run_celery_worker.sh`,
    `./webapp/run_redis_server.sh` and `./webapp/run_webapp.sh`, one per terminal. You now should have a celery
    worker, a redis server and the flask server up and running. Connect to it with the following
    link: `http://127.0.0.1:5000/`.

In the end, your directory structure should look like this:

```bash
.
├── venv/
├── terminal/
├── tests/
├── util/
├── webapp/
│   ├── application/
│   │   ├── blueprints/
│   │   ├── static/
│   │   ├── templates/
│   │   ├── .env
│   │   ├── __init__.py
│   │   ├── app.py
│   │   ├── config.json
│   │   ├── config.py
│   │   ├── db.py
│   │   ├── extensions.py
│   │   ├── models.py
│   │   ├── socketio_emits.py
│   │   ├── tasks.py
│   │   └── twitch_bot.py
│   ├── redis-6.2.6/ # Or and newer version
│   ├── screenshots/
│   ├── README.md
│   ├── run_celery_worker.sh
│   ├── run_redis_server.sh
│   ├── run_webapp.sh
│   └── wsgi.py
├── .gitignore
├── CHANGELOG.md
├── LICENSE.md
├── README.md
├── requirements_dev.txt
├── requirements_terminal.txt
└── requirements_webapp.txt
```

## Configuration

TwitchBot can be configured with the `config.json` file:

- the `SPAM_LIST` array sets which words/phrases should be considered spam (**important**: the entries must be in lower
  case). Python's regex is fully supported.
- `USERNAME` sets the name of the TwitchBot.
- `CHANNEL` sets to which Twitch.tv channel TwitchBot should connect to.
- `BOT_COLOUR_NORMAL` sets TwitchBot's chat message colour for subscriptions, re-subscriptions, gifted subscriptions and
  raids.
- `BOT_COLOUR_WARNING_TIMEOUT` sets TwitchBot's chat message colour for warnings and timeouts.
- `DEFAULT_USERNAME_COLOUR` sets the colour of the associated Twitch.tv account (useful when using the same Twitch.tv
  account for TwitchBot and the human user).

Note that the colours must be in hex (#000000) format or one of the following values: Blue, BlueViolet, CadetBlue,
Chocolate, Coral, DodgerBlue, Firebrick, GoldenRod, Green, HotPink, OrangeRed, Red, SeaGreen, SpringGreen, YellowGreen.

- lastly, the `INFO_CARDS` array sets the following attributes for the information cards on the Twitch Bot page:
    - `position` (at which position the info card appears; possible values: 1 through 8)
    - `header` (the title of the info card; possible values: any string or integer)
    - `data_type` (what kind of data the info card holds; possible values: `bot_status`, `bot_uptime`,
      `connected_channel`, `messages_count`, `new_subscriptions_count`, `raid_count`, `bot_warnings_count`,
      `bot_timeouts_count`)
    - `data_value` (the default value the info card should hold when resetting the database; possible values: any string
      or integer)
    - `html_additional_class` (the default css class(es) the info card should have in addition to the ones set
      in `macros.html`; possible values: any string)

## Known Issues

When stopping the IRC bot the celery server might show an error like this:

```bash
Task handler raised error: Terminated(15)
...
raise Terminated(-(signum or 0))
billiard.exceptions.Terminated: 15
```

This is in fact not an error but a log message and no exception is raised at all in the worker (see
[this](https://github.com/celery/celery/issues/2727#issuecomment-777775437) GitHub issue's comment). It doesn't happen
every time and can be ignored.