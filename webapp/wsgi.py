# -*- coding: utf-8 -*-
"""This file contains the entry point to the webapp."""

from webapp.application import app, db
from webapp.application.extensions import celery, database, socketio

app = app.create_app()


@app.cli.command('initdb')
def reset_database():
    print('\nInitialising database ...')

    db.initialise_database(database)

    print('\nSuccess')


if __name__ == '__main__':
    socketio.run(app)
