# -*- coding: utf-8 -*-
"""This file contains functions for the database."""

# All database models need to be imported, otherwise the `create_all()` function
# creates an empty database without any tables.
from webapp.application.models import TwitchBotInfoCards, TwitchBotConnectionOverview, TwitchBotMessages
from util import json_util, list_util


def initialise_database(database) -> None:
    """
    This function resets the database and initialises it with the initial values
    for the info cards.
    """
    database.drop_all()
    database.create_all()

    sorted_info_cards: list = list_util.sort_list_of_dictionaries_by_value(
        json_util.read_file('./webapp/application/config.json')['INFO_CARDS'], 'position')

    positions: list = list_util.get_values_from_list_of_dictionaries(sorted_info_cards, 'position')
    headers: list = list_util.get_values_from_list_of_dictionaries(sorted_info_cards, 'header')
    data_types: list = list_util.get_values_from_list_of_dictionaries(sorted_info_cards, 'data_type')
    data_values: list = list_util.get_values_from_list_of_dictionaries(sorted_info_cards, 'data_value')
    html_additional_classes: list = list_util.get_values_from_list_of_dictionaries(sorted_info_cards,
                                                                                   'html_additional_class')

    # Add data to the `TwitchBotInfoCards` table
    for position, header, data_type, data_value, html_additional_class in zip(positions, headers, data_types,
                                                                              data_values, html_additional_classes):
        database.session.add(
            TwitchBotInfoCards(position=position, header=str(header), data_type=str(data_type),
                               data_value=str(data_value), html_additional_class=str(html_additional_class)))

    database.session.commit()
