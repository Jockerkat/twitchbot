# -*- coding: utf-8 -*-
"""This file holds all the socketio emits used by the webapp."""

import json
from webapp.application.extensions import socketio


# Info Cards
def update_bot_status(data: str) -> None:
    """
    This function emits data using the 'update_bot_status' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_bot_status', data=data, namespace='/info_card', broadcast=True)


def update_bot_uptime(data: str) -> None:
    """
    This function emits data using the 'update_bot_uptime' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_bot_uptime', data=data, namespace='/info_card', broadcast=True)


def update_connected_channel(data: str) -> None:
    """
    This function emits data using the 'update_connected_channel' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_connected_channel', data=data, namespace='/info_card', broadcast=True)


def update_messages_count(data: str) -> None:
    """
    This function emits data using the 'update_messages_count' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_messages_count', data=data, namespace='/info_card', broadcast=True)


def update_new_subscriptions_count(data: str) -> None:
    """
    This function emits data using the 'update_new_subscriptions_count' event
    and '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_new_subscriptions_count', data=data, namespace='/info_card', broadcast=True)


def update_raid_count(data: str) -> None:
    """
    This function emits data using the 'update_raid_count' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_raid_count', data=data, namespace='/info_card', broadcast=True)


def update_bot_warnings_count(data: str) -> None:
    """
    This function emits data using the 'update_bot_warnings_count' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_bot_warnings_count', data=data, namespace='/info_card', broadcast=True)


def update_bot_timeouts_count(data: str) -> None:
    """
    This function emits data using the 'update_bot_timeouts_count' event and
    '/info_card' namespace.

    :param data: the data which should be emitted
    """

    socketio.emit('update_bot_timeouts_count', data=data, namespace='/info_card', broadcast=True)


# Connection Overview
def update_connection_overview(data: dict) -> None:
    """
    This function emits data using the 'update_connection_overview' event and
    '/connection_overview'
    namespace.

    :param data: the data which should be emitted; it will be formatted as JSON
                 and any objects which aren't serializable are converted to a
                 string
    """

    socketio.emit('update_connection_overview',
                  data=json.dumps(data, default=str),
                  namespace='/connection_overview',
                  broadcast=True)


# Messages
def update_messages(data: dict) -> None:
    """
    This function emits data using the 'update_messages' event and '/messages'
    namespace.

    :param data: the data which should be emitted; it will be formatted as JSON
                 and any objects which aren't serializable are converted to a
                 string
    """

    socketio.emit('update_messages',
                  data=json.dumps(data, default=str),
                  namespace='/messages',
                  broadcast=True)
