# -*- coding: utf-8 -*-
"""This file contains the home page's blueprint and routes."""

from flask import Blueprint, render_template

# Create blueprint
home_page_app = Blueprint(
    'home_page_app',
    __name__,
    static_folder='../static/',
    template_folder='../templates/'
)


# Routes
@home_page_app.route('/')
def home_page():
    """
    This function routes to the root of the blueprint, which is ``home.html``.
    """

    return render_template('home.html')
