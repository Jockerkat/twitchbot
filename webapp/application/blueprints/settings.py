# -*- coding: utf-8 -*-
"""This file contains the home page's blueprint and routes."""

from flask import Blueprint, render_template
from util import json_util, list_util
from webapp.application.extensions import socketio

# Create blueprint
settings_page_app = Blueprint(
    'settings_page_app',
    __name__,
    static_folder='../static/',
    template_folder='../templates/'
)


# Routes
@settings_page_app.route('/')
def home_page():
    """
    This function routes to the root of the blueprint, which is ``settings.html``.
    """

    json_data = json_util.read_file('./webapp/application/config.json')

    return render_template('settings.html',
                           spam_list=json_data['SPAM_LIST'],
                           username=json_data['USERNAME'],
                           channel=json_data['CHANNEL'],
                           bot_colour_normal=json_data['BOT_COLOUR_NORMAL'],
                           bot_colour_warning_timeout=json_data['BOT_COLOUR_WARNING_TIMEOUT'],
                           default_username_colour=json_data['DEFAULT_USERNAME_COLOUR'],
                           info_cards=json_data['INFO_CARDS'])


# SocketIO
@socketio.on('receive_basic_settings_update')
def receive_basic_settings_update(json):
    """This function receives the updated values for the bot's basic settings."""

    if not json:
        return

    for key, value in zip(list_util.get_values_from_list_of_dictionaries(json, 'ID'),
                          list_util.get_values_from_list_of_dictionaries(json, 'value')):
        json_util.update_value('./webapp/application/config.json', key.upper(), str(value))

    socketio.emit('settings_update_success')


@socketio.on('receive_advanced_settings_update')
def receive_advanced_settings_update(json):
    """This function receives the updated values for the bot's advanced settings."""

    if not json:
        return

    spam_list_json_data = json_util.read_file('./webapp/application/config.json')['SPAM_LIST']

    # Update SPAM_LIST
    for key, value in zip(list_util.get_values_from_list_of_dictionaries(json, 'ID'),
                          list_util.get_values_from_list_of_dictionaries(json, 'value')):

        # Get index of spam list items to be updated
        index = int(key[-1])

        # If the key's index (the number at the end of its ID, e.g., if the ID is 'spam_list_4',
        # the ID is 4) is less than the length of the SPAM_LIST array, the value in the array at
        # the corresponding index should be updated. Otherwise, append the new element to the
        # SPAM_LIST array.
        if index < len(spam_list_json_data):
            # If the value is empty it should be removed from the SPAM_LIST array
            if value == '':
                json_util.delete_value_in_array('./webapp/application/config.json',
                                                'SPAM_LIST',
                                                index)
            else:
                json_util.update_value_in_array('./webapp/application/config.json',
                                                'SPAM_LIST',
                                                index,
                                                str(value))
        else:
            json_util.append_value_to_array('./webapp/application/config.json',
                                            'SPAM_LIST',
                                            str(value))

    socketio.emit('settings_update_success')
