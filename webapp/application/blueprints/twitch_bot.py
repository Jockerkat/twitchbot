# -*- coding: utf-8 -*-
"""This file contains the home page's blueprint and routes."""

from flask import Blueprint, render_template
from webapp.application.models import TwitchBotInfoCards, TwitchBotConnectionOverview, TwitchBotMessages
from webapp.application.extensions import socketio, celery

# Create blueprint
twitch_bot_page_app = Blueprint(
    'twitch_bot_page_app',
    __name__,
    static_folder='../static/',
    template_folder='../templates/'
)

# Table headers
CONNECTION_OVERVIEW_TABLE_HEADERS = ('UTC Timestamp', 'IRC Connection Message')
MESSAGES_TABLE_HEADERS = ('UTC Timestamp', 'Event', 'Spam Flag', 'Author', 'Message')


# Get data
def get_info_card_headers():
    """This function queries the database for the info card headers."""

    headers = TwitchBotInfoCards.query.with_entities(TwitchBotInfoCards.header).all()
    return [header[0] for header in headers]


def get_info_card_data_types():
    """This function queries the database for the info card data types."""

    data_types = TwitchBotInfoCards.query.with_entities(TwitchBotInfoCards.data_type).all()
    return [data_type[0] for data_type in data_types]


def get_info_card_data_values():
    """This function queries the database for the info card data values."""

    data_values = TwitchBotInfoCards.query.with_entities(TwitchBotInfoCards.data_value).all()
    return [data_value[0] for data_value in data_values]


def get_info_card_html_additional_classes():
    """This function queries the database for the info card html additional classes."""

    html_additional_classes = TwitchBotInfoCards.query. \
        with_entities(TwitchBotInfoCards.html_additional_class).all()
    return [html_additional_class[0] for html_additional_class in html_additional_classes]


def get_connection_overview_timestamps():
    """
    This function queries the database for the connection overview timestamps in
    descending order.
    """

    timestamps = TwitchBotConnectionOverview.query. \
        with_entities(TwitchBotConnectionOverview.timestamp). \
        order_by(TwitchBotConnectionOverview.id.desc()).all()
    return [timestamp[0] for timestamp in timestamps]


def get_connection_overview_messages():
    """
    This function queries the database for the connection overview messages in
    descending order.
    """

    messages = TwitchBotConnectionOverview.query. \
        with_entities(TwitchBotConnectionOverview.message). \
        order_by(TwitchBotConnectionOverview.id.desc()).all()
    return [message[0] for message in messages]


def get_messages_timestamps():
    """
    This function queries the database for the messages' timestamps in descending
    order.
    """

    timestamps = TwitchBotMessages.query.with_entities(TwitchBotMessages.timestamp). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [timestamp[0] for timestamp in timestamps]


def get_messages_events():
    """
    This function queries the database for the messages' events in descending
    order.
    """

    events = TwitchBotMessages.query.with_entities(TwitchBotMessages.event). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [event[0] for event in events]


def get_messages_spam_flags():
    """
    This function queries the database for the messages' spam flags in descending
    order.
    """

    spam_flags = TwitchBotMessages.query.with_entities(TwitchBotMessages.spam_flag). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [spam_flag[0] for spam_flag in spam_flags]


def get_messages_authors():
    """
    This function queries the database for the messages' authors in descending
    order.
    """

    authors = TwitchBotMessages.query.with_entities(TwitchBotMessages.author). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [author[0] for author in authors]


def get_messages_messages():
    """
    This function queries the database for the messages' messages in descending
    order.
    """

    messages = TwitchBotMessages.query.with_entities(TwitchBotMessages.message). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [message[0] for message in messages]


def get_messages_html_additional_classes():
    """
    This function queries the database for the messages' html additional classes
    in descending order.
    """

    html_additional_classes = TwitchBotMessages.query. \
        with_entities(TwitchBotMessages.html_additional_class). \
        order_by(TwitchBotMessages.id.desc()).all()
    return [html_additional_class[0] for html_additional_class in html_additional_classes]


# Routes
@twitch_bot_page_app.route('/')
def home_page():
    """
    This function routes to the root of the blueprint, which is ``twitch_bot.html``.
    """

    # The zip function is not supported in jinja2, which is why it is passed as a template variable.
    # Inspiration: https://stackoverflow.com/a/49447550
    return render_template('twitch_bot.html',
                           info_card_headers=get_info_card_headers(),
                           info_card_data_types=get_info_card_data_types(),
                           info_card_data_values=get_info_card_data_values(),
                           info_card_html_additional_classes=get_info_card_html_additional_classes(),
                           connection_overview_table_headers=CONNECTION_OVERVIEW_TABLE_HEADERS,
                           connection_overview_timestamps=get_connection_overview_timestamps(),
                           connection_overview_messages=get_connection_overview_messages(),
                           messages_table_headers=MESSAGES_TABLE_HEADERS,
                           messages_timestamps=get_messages_timestamps(),
                           messages_events=get_messages_events(),
                           messages_spam_flags=get_messages_spam_flags(),
                           messages_authors=get_messages_authors(),
                           messages_messages=get_messages_messages(),
                           messages_html_additional_classes=get_messages_html_additional_classes(),
                           zip=zip)


# SocketIO
@socketio.on('twitch_bot_start')
def twitch_bot_start():
    """This function starts the Twitch Bot and the uptime stopwatch."""

    celery.send_task('twitch_bot_start')


@socketio.on('twitch_bot_stop')
def twitch_bot_stop():
    """This function stops the Twitch Bot and the uptime stopwatch."""

    celery.send_task('twitch_bot_stop')
