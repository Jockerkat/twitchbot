# -*- coding: utf-8 -*-
"""This file contains all the extensions used by the TwitchBot webapp."""

from celery import Celery
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from webapp.application.config import BaseConfig

celery: Celery = Celery(
    __name__,
    backend=BaseConfig.CELERY_RESULT_BACKEND,
    broker=BaseConfig.CELERY_BROKER_URL,
    include=['application.tasks']
)
socketio: SocketIO = SocketIO()
database: SQLAlchemy = SQLAlchemy()
