# -*- coding: utf-8 -*-
"""This file holds all the celery tasks used by the webapp."""

from datetime import datetime
import time
import os
from dotenv import load_dotenv
from billiard import exceptions
from util import json_util
from webapp.application.twitch_bot import TwitchBot
from webapp.application.extensions import celery, database
from webapp.application.models import TwitchBotInfoCards, TwitchBotConnectionOverview
from webapp.application import socketio_emits


@celery.task(name='twitch_bot_uptime_stopwatch', throws=exceptions.Terminated)
def twitch_bot_uptime_stopwatch():
    """
    This celery function calculates the time the bot has been active (HH:MM:SS)
    and emits it to the client. Every five seconds, the value gets saved to the
    database.
    """

    try:
        elapsed_time: int = 0

        while True:
            hours, remainder = divmod(elapsed_time, 3600)
            minutes, seconds = divmod(remainder, 60)
            stopwatch: str = '{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds)
            elapsed_time += 1

            # Update client
            socketio_emits.update_bot_uptime(stopwatch)

            # Update database
            if seconds % 5 == 0:
                TwitchBotInfoCards.query.filter_by(data_type='bot_uptime').first().data_value = stopwatch
                database.session.commit()

            time.sleep(1)

    except SystemExit:
        print('INFO: Uptime stopwatch stopped successfully')


@celery.task(name='twitch_bot_start', throws=exceptions.Terminated)
def start():
    """This celery function starts the IRC bot."""

    try:
        load_dotenv(dotenv_path='./application/.env')

        config: dict = json_util.read_file('./application/config.json')

        username: str = config['USERNAME']
        client_id: str = os.getenv('CLIENT_ID')
        client_secret: str = os.getenv('CLIENT_SECRET')
        token: str = os.getenv('TOKEN')
        channel: str = config['CHANNEL']

        # Update client and database
        data = {'timestamp': datetime.utcnow(), 'message': 'Starting twitch bot ...'}

        socketio_emits.update_connection_overview(data)
        database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))
        database.session.commit()

        TwitchBot(username, client_id, client_secret, token, channel).start()

    except SystemExit:
        print('INFO: Twitch Bot stopped successfully')


@celery.task(name='twitch_bot_stop', throws=exceptions.Terminated)
def stop():
    """This celery function stops the IRC bot."""

    # Update client and database: Connection overview, bot status and connected channel
    data: dict = {'timestamp': datetime.utcnow(), 'message': 'Stopping twitch bot ...'}

    socketio_emits.update_connection_overview(data)
    database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))
    database.session.commit()

    # When revoking the task(s), celery throws the following error:
    #   `Task handler raised error: Terminated(15)`
    #   `[...]`
    #   `raise Terminated(-(signum or 0))`
    #   `billiard.exceptions.Terminated: 15`
    # However, this seems to be more of an information than a hard error
    # (see https://github.com/celery/celery/issues/2727#issuecomment-777775437), as celery
    # continues to work fine, even after throwing this error. So for now this is a "feature" :)
    #
    # Putting a try/except SystemExit block works for the first time, but not always for subsequent
    # task cancellations (inspiration: https://stackoverflow.com/a/67054139). Dunno why.
    #
    # Inspiration: https://gist.github.com/kgantsov/85be7251b42767d229cd0e1adb48b7a2
    worker_prefix: str = ''
    task_names: list = ['twitch_bot_start', 'twitch_bot_uptime_stopwatch']

    for worker_name, tasks in celery.control.inspect().active().items():
        if worker_name.startswith(worker_prefix):
            for task_name in task_names:
                for task in tasks:
                    if task['name'] == task_name:
                        celery.control.revoke(task['id'], terminate=True, signal='TERM')

    data: dict = {'timestamp': datetime.utcnow(), 'message': 'Stopped successfully'}

    # Update client
    socketio_emits.update_bot_status('inactive')
    socketio_emits.update_connected_channel('N/A')
    socketio_emits.update_bot_uptime('N/A')
    socketio_emits.update_connection_overview(data)

    # Update database
    bot_status = TwitchBotInfoCards.query.filter_by(data_type='bot_status').first()
    bot_status.data_value = 'Inactive'
    bot_status.html_additional_class = 'information__card__data__inactive'

    TwitchBotInfoCards.query.filter_by(data_type='connected_channel').first().data_value = 'N/A'
    TwitchBotInfoCards.query.filter_by(data_type='bot_uptime').first().data_value = 'N/A'

    database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))
    database.session.commit()
