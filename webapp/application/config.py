# -*- coding: utf-8 -*-
"""This file contains all the configuration for the TwitchBot webapp."""

import os


class BaseConfig:
    """
    The BaseConfig provides the parameters for the Flask application.

    The following parameters are configured:

    -  PROJECT_ROOT
    -  CELERY_BROKER_URL
    -  CELERY_RESULT_BACKEND
    -  SOCKETIO_MESSAGE_QUEUE
    -  SQLALCHEMY_DATABASE_URI
    -  SQLALCHEMY_TRACK_MODIFICATIONS
    -  SECRET_KEY

    """

    PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    CELERY_BROKER_URL: str = 'redis://localhost:6379'
    CELERY_RESULT_BACKEND: str = 'redis://localhost:6379'

    SOCKETIO_MESSAGE_QUEUE: str = 'redis://'

    SQLALCHEMY_DATABASE_URI: str = 'sqlite:///twitch_bot.sqlite3'
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False

    SECRET_KEY: str = 'some-super-secret-key-here'
