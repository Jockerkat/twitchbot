const socket = io();

socket.on('settings_update_success', function(message) {
    // Create an alert when settings changes were successful and reload the page
    // so the changes are reflected properly
    alert("Successfully updated settings.");
    location.reload();
});

function updateChangedSettings(settingsIDArray, socketioEvent) {
    // Create empty array which holds the changed settings
    const updates = [];

    // Loop through array
    settingsIDArray.forEach((ID) => {
        // Get element reference
        let setting = document.getElementById(ID);

        // Check if value is different from the initial value
        if (setting.value != setting.defaultValue) {
            // Add change to array
            updates.push({"ID":`${ID}`, "value":`${setting.value}`});
        }
    });
    // Send update to back end
    socket.emit(socketioEvent, updates);
}

function resetSettingValueToDefault(settingsIDArray) {
    // Loop through array
    settingsIDArray.forEach((ID) => {
        // Get element reference
        let setting = document.getElementById(ID);

        // Set value of input field back to default
        setting.value = setting.defaultValue;

        // Update element class
        updateClassOnSettingChange(ID);
    });
}

function updateClassOnSettingChange(elementID) {
    // Get element reference
    let element = document.getElementById(elementID);

    // If value is different from the initial value, add class, else remove
    if (element.value != element.defaultValue) {
        // Add class 'setting__changed'
        element.classList.add('setting__changed');
    }
    else if (element.value == element.defaultValue) {
        // Remove class 'setting__changed'
        element.classList.remove('setting__changed');
    }
}

function addNewSpamListInputField() {
    // Get reference to container
    let container = document.getElementsByClassName('main__body__settings__advanced__settings__spam_list')[0];

    // Get amount of spam list entries already present
    spam_list_entries = countChildElements('main__body__settings__advanced__settings__spam_list', 'input');

    // Create input field
    let input_field = document.createElement('input');
    input_field.type = 'text';
    input_field.id = 'spam_list_' + String(spam_list_entries);
    input_field.defaultValue = '';
    input_field.setAttribute('onchange', 'updateClassOnSettingChange(this.id)');

    // Add input field at the top of the container
    container.append(input_field);
}

function countChildElements(parentElementClassName, childElementType) {
    // Get child elements
    let child_elements = getChildElements(parentElementClassName, childElementType);
    
    return child_elements.length;
}

function getChildElements(parentElementClassName, childElementType) {
    // Get reference to parent container
    let container = document.getElementsByClassName(parentElementClassName)[0];

    // Count the children of the same type
    let everyChildElement = container.querySelectorAll(childElementType);

    return everyChildElement;
}

function getIDsFromChildElements(parentElementClassName, childElementType) {
    // Create empty array which holds the IDs
    const IDs = [];

    // Get child elements
    let child_elements = getChildElements(parentElementClassName, childElementType);

    // Get IDs from child elements and add them to the IDs array
    child_elements.forEach((child_element) => {
        IDs.push(child_element.id);
    });

    return IDs;
}