function getSubstringBetweenCharacters(string, delimiter) {
    // This function returns the first substring between a given delimiter, e.g.
    // if the parameter 'string' is "/home/test/" and the parameter 'delimiter' is
    // '/', the returned value will be "home"

    // Get position of the first and second occurrence of delimiter
    let firstDelimiterOccurrence = string.indexOf(delimiter);
    let secondDelimiterOccurrence = string.indexOf(delimiter, firstDelimiterOccurrence + 1);

    return string.substring(firstDelimiterOccurrence + 1, secondDelimiterOccurrence);
}

function updateInfoCardData(infoCardDataClass, infoCardNewData) {
    // Get reference to html element
    let infoCardData = document.getElementsByClassName(infoCardDataClass)[0];

    // Change inner html content
    infoCardData.innerHTML = infoCardNewData;
}

function addJSONDataToNewTableRow(tableID, rowData, dataRowClass = '') {
    // Inspiration: https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement/insertRow

    // Get a reference to the table
    let tableReference = document.getElementById(tableID);

    // Insert a row at the top of the table
    let newRow = tableReference.insertRow(1);

    // Add class 'table__row' to new row
    newRow.classList.add('table__row');

    // Loop through data
    let i = 0;

    for (const key in rowData) {
        if (rowData.hasOwnProperty(key)) {
            // Insert a cell in the row
            let newCell = newRow.insertCell(i);

            // Add class 'table__cell' to new cell
            if (dataRowClass === '') {
                newCell.classList.add('table__cell');
            }
            else {
                newCell.classList.add('table__cell', dataRowClass);
            }

            // Append a text node to the cell
            let newText = document.createTextNode(rowData[key]);
            newCell.appendChild(newText);
        }
        i++
    }
}

export { updateInfoCardData, getSubstringBetweenCharacters, addJSONDataToNewTableRow };