import { getSubstringBetweenCharacters } from './util.js';

function addClassActiveToNavbar() {
    // Get current path name
    let pathname = new URL(document.URL).pathname;

    // Get current path name without the '/' at the beginning and end
    let page = getSubstringBetweenCharacters(pathname, '/');

    // Get a reference to the new active element
    let newActiveElement = document.getElementsByClassName('nav__links__' + page);

    // Add class name
    newActiveElement[0].classList.add('nav__links__active');
}

addClassActiveToNavbar();