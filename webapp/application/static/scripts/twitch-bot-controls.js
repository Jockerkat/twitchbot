const socket = io();

function twitch_bot_start() {
    socket.emit('twitch_bot_start');
}

function twitch_bot_stop() {
    socket.emit('twitch_bot_stop');
}