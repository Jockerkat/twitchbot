import { updateInfoCardData, addJSONDataToNewTableRow } from './util.js';

const infoCardSocket = io('/info_card');
const connectionOverviewSocket = io('/connection_overview');
const messagesSocket = io('/messages');

// Info Cards
infoCardSocket.on('update_bot_status', function(message) {
    // Refer to html element
    let infoCardData = document.getElementsByClassName('information__card__data bot_status')[0];

    if (message === 'active')  {
        // Remove class inactive
        infoCardData.classList.remove('information__card__data__inactive');

        // Add class active
        infoCardData.classList.add('information__card__data__active');

        // Change inner html to active
        infoCardData.innerHTML = 'Active';
    }
    else if (message === 'inactive') {
        // Remove class active
        infoCardData.classList.remove('information__card__data__active');

        // Add class inactive
        infoCardData.classList.add('information__card__data__inactive');

        // Change inner html to inactive
        infoCardData.innerHTML = 'Inactive';
    }
});

infoCardSocket.on('update_bot_uptime', function(message) {
    updateInfoCardData('information__card__data bot_uptime', message);
});

infoCardSocket.on('update_connected_channel', function(message) {
    updateInfoCardData('information__card__data connected_channel', message);
});

infoCardSocket.on('update_messages_count', function(message) {
    updateInfoCardData('information__card__data messages_count', message);
});

infoCardSocket.on('update_new_subscriptions_count', function(message) {
    updateInfoCardData('information__card__data new_subscriptions_count', message);
});

infoCardSocket.on('update_raid_count', function(message) {
    updateInfoCardData('information__card__data raid_count', message);
});

infoCardSocket.on('update_bot_warnings_count', function(message) {
    updateInfoCardData('information__card__data bot_warnings_count', message);
});

infoCardSocket.on('update_bot_timeouts_count', function(message) {
    updateInfoCardData('information__card__data bot_timeouts_count', message);
});

// Connection overview
connectionOverviewSocket.on('update_connection_overview', function(message) {
    addJSONDataToNewTableRow('table__connection_overview', JSON.parse(message));
});

// Messages
messagesSocket.on('update_messages', function(message) {
    let jsonMessage = JSON.parse(message)
    let dataRowClass = jsonMessage.data_row_class

    // Remove key 'data_row_class' from JSON object as this shouldn't be added to the table
    delete jsonMessage.data_row_class;

    addJSONDataToNewTableRow('table__messages', jsonMessage, dataRowClass);
});