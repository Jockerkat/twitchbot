# -*- coding: utf-8 -*-
"""This file contains the TwitchBot class."""

from datetime import datetime
import irc.bot
import irc.client
from webapp.application.extensions import celery, database
from util import datetime_util, twitch_api_util, twitch_chat_util, json_util
from webapp.application.models import TwitchBotConnectionOverview, TwitchBotMessages, TwitchBotInfoCards
from webapp.application import socketio_emits


class TwitchBot(irc.bot.SingleServerIRCBot):
    """This class is responsible for connecting to a twitch channel and receiving messages."""

    def __init__(self, username: str, client_id: str, client_secret: str, token: str, channel: str):
        self.username: str = username
        self.client_id: str = client_id
        self.client_secret: str = client_secret
        self.token: str = token
        self.channel: str = '#' + channel
        self.config = None

        self.messages_count: int = 0
        self.new_subscriptions_count: int = 0
        self.raid_count: int = 0
        self.bot_warnings_count: int = 0
        self.bot_timeouts_count: int = 0

        # Authorise with the Twitch API
        app_access_token: dict = twitch_api_util.get_app_access_token(self.client_id, self.client_secret)
        stream_information: dict = twitch_api_util.get_stream_information(self.client_id, app_access_token, channel)

        # Initialise IRC bot connection (connect to chat)
        server: str = 'irc.chat.twitch.tv'
        port: int = 6667

        # Update client and database
        data = {'timestamp': datetime.utcnow(), 'message': f'Connecting to {server} on port {str(port)} ...'}

        socketio_emits.update_connection_overview(data)
        database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))
        database.session.commit()

        # Initialise IRC bot
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, 'oauth:' + self.token)], username, username)

    def on_welcome(self, connection: irc.client.ServerConnection, event: irc.client.Event):
        """This function is triggered when the bot connects to a twitch channel."""

        # Update client and database
        data = {'timestamp': datetime.utcnow(), 'message': f'Joining channel {self.channel} ...'}

        socketio_emits.update_connection_overview(data)
        database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))
        database.session.commit()

        # Request specific capabilities so they can be used
        connection.cap('REQ', ':twitch.tv/membership')
        connection.cap('REQ', ':twitch.tv/tags')
        connection.cap('REQ', ':twitch.tv/commands')

        connection.join(self.channel)

        # Update client and database: Connection overview, bot status and connected channel
        data = {'timestamp': datetime.utcnow(), 'message': 'Joined successfully'}

        socketio_emits.update_connection_overview(data)
        socketio_emits.update_bot_status('active')
        socketio_emits.update_connected_channel(self.channel)

        database.session.add(TwitchBotConnectionOverview(timestamp=data['timestamp'], message=data['message']))

        bot_status = TwitchBotInfoCards.query.filter_by(data_type='bot_status').first()
        bot_status.data_value = 'Active'
        bot_status.html_additional_class = 'information__card__data__active'
        TwitchBotInfoCards.query.filter_by(data_type='connected_channel').first().data_value = self.channel

        database.session.commit()

        # Read `config.json`.
        # This is done here instead of in the `__init__` function as celery "pre-runs" all its tasks
        # and thus the `__init__` function, meaning changes to the `config.json` won't be
        # reflected until the celery worker is restarted.
        self.config: dict = json_util.read_file('./application/config.json')

        celery.send_task('twitch_bot_uptime_stopwatch')

        # Set colour of account to default
        twitch_chat_util.change_chat_message_colour(connection, self.channel, self.config['DEFAULT_USERNAME_COLOUR'])

    def on_pubmsg(self, connection: irc.client.ServerConnection, event: irc.client.Event):
        """This function is triggered when someone sends a chat message in the connected channel."""
        tags: dict = twitch_chat_util.get_event_tags(event)

        timestamp: datetime = datetime_util.posix_timestamp_to_datetime(int(tags['tmi-sent-ts']))
        author: str = tags['display-name']
        message: str = event.arguments[0]

        is_spam: bool = twitch_chat_util.chat_message_is_spam(twitch_chat_util.strip_unicode_characters(message),
                                                              self.config['SPAM_LIST'])

        if is_spam:
            data = {'timestamp': timestamp,
                    'event': 'pubmsg',
                    'spam_flag': 'Potential Spam',
                    'author': author,
                    'message': message,
                    'data_row_class': 'potential__spam'}

            self.bot_warnings_count += 1

            socketio_emits.update_bot_warnings_count(str(self.bot_warnings_count))
            database.session.add(TwitchBotMessages(timestamp=timestamp,
                                                   event='pubmsg',
                                                   spam_flag='Potential Spam',
                                                   author=author,
                                                   message=message,
                                                   html_additional_class='potential__spam'))
            TwitchBotInfoCards.query.filter_by(
                data_type='bot_warnings_count').first().data_value = self.bot_warnings_count

            # Send warning in twitch chat
            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['BOT_COLOUR_WARNING_TIMEOUT'])
            twitch_chat_util.send_chat_message(connection,
                                               self.channel,
                                               f"/me @{author}, your most recent chat "
                                               "message has been detected as potential spam "
                                               "(I'm a bot MrDestructoid )")
            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['DEFAULT_USERNAME_COLOUR'])

        else:
            data = {'timestamp': timestamp,
                    'event': 'pubmsg',
                    'spam_flag': 'Okay',
                    'author': author,
                    'message': message}

            database.session.add(TwitchBotMessages(timestamp=timestamp,
                                                   event='pubmsg',
                                                   spam_flag='Okay',
                                                   author=author,
                                                   message=message,
                                                   html_additional_class=''))

        self.messages_count += 1

        # Update client and database: Messages read, warnings issued if spam, messages table
        socketio_emits.update_messages(data)
        socketio_emits.update_messages_count(str(self.messages_count))
        TwitchBotInfoCards.query.filter_by(data_type='messages_count').first().data_value = self.messages_count

        database.session.commit()

    def on_usernotice(self, connection: irc.client.ServerConnection, event: irc.client.Event):
        """This function is triggered when someone raids, subscribes, etc."""
        tags: dict = twitch_chat_util.get_event_tags(event)

        event: str = tags['msg-id']
        timestamp: datetime = datetime_util.posix_timestamp_to_datetime(int(tags['tmi-sent-ts']))
        event_author: str = tags['login'] if tags['display-name'] == '' else tags['display-name']
        event_data: dict = {}

        # Update client and database: Messages read, raid/sub count, messages table
        if event == 'raid':
            event_data = {'event_type': 'Raid',
                          'event_data_row_class': 'usernotice__raid',
                          'messages': [f"/me Thank you @{event_author} for the raid! "
                                       "deckzEnergy ",
                                       f"/me !so {event_author}"]}
            # Update info card and database
            self.raid_count += 1
            socketio_emits.update_new_subscriptions_count(str(self.raid_count))
            TwitchBotInfoCards.query.filter_by(data_type='raid_count'). \
                first().data_value = self.raid_count
        elif event == 'sub':
            event_data = {'event_type': 'Sub',
                          'event_data_row_class': 'usernotice__sub',
                          'messages': [f"/me Thank you @{event_author} for the sub! "
                                       "deckzSUBGANG "]}
            # Update info card and database
            self.new_subscriptions_count = self.new_subscriptions_count + 1
            socketio_emits.update_new_subscriptions_count(str(self.new_subscriptions_count))
            TwitchBotInfoCards.query.filter_by(
                data_type='new_subscriptions_count').first().data_value = self.new_subscriptions_count
        elif event == 'resub':
            event_data = {'event_type': 'Resub',
                          'event_data_row_class': 'usernotice__resub',
                          'messages': [f"/me Thank you @{event_author} for the resub! "
                                       "deckzSUBGANG "]}
            # Update info card and database
            self.new_subscriptions_count += 1
            socketio_emits.update_new_subscriptions_count(str(self.new_subscriptions_count))
            TwitchBotInfoCards.query.filter_by(
                data_type='new_subscriptions_count').first().data_value = self.new_subscriptions_count
        elif event == 'subgift':
            event_data = {'event_type': 'Subgift',
                          'event_data_row_class': 'usernotice__subgift',
                          'messages': [f"/me Thank you @{event_author} for the gifted "
                                       "sub! deckzSUBGANG "]}
            # Update info card and database
            self.new_subscriptions_count += 1
            socketio_emits.update_new_subscriptions_count(str(self.new_subscriptions_count))
            TwitchBotInfoCards.query.filter_by(
                data_type='new_subscriptions_count').first().data_value = self.new_subscriptions_count
        else:
            print(f"event {event} occurred with tags {tags}")

        # If the event isn't handled above, the dictionary `event_data` is empty, and evaluates to False
        if bool(event_data):
            # Send message(s) in chat
            twitch_chat_util.change_chat_message_colour(connection, self.channel, self.config['BOT_COLOUR_NORMAL'])

            for message in event_data['messages']:
                twitch_chat_util.send_chat_message(connection, self.channel, message)

            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['DEFAULT_USERNAME_COLOUR'])

            # Update info card
            self.messages_count += 1
            socketio_emits.update_messages_count(str(self.messages_count))

            # Update messages table and database
            data = {'timestamp': timestamp,
                    'event': event_data['event_type'],
                    'spam_flag': 'Okay',
                    'author': event_author,
                    'message': 'N/A',
                    'data_row_class': event_data['event_data_row_class']}
            socketio_emits.update_messages(data)

            database.session.add(TwitchBotMessages(timestamp=timestamp,
                                                   event=event_data['event_type'],
                                                   spam_flag='Okay',
                                                   author=event_author,
                                                   message='N/A',
                                                   html_additional_class=event_data['event_data_row_class']))

            TwitchBotInfoCards.query.filter_by(data_type='messages_count').first().data_value = self.messages_count
            database.session.commit()
