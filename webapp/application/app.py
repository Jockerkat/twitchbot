# -*- coding: utf-8 -*-
"""This file contains all the factories for the webapp."""

from flask import Flask, redirect, url_for, render_template
from webapp.application.config import BaseConfig
from webapp.application.extensions import celery, database, socketio
from webapp.application.blueprints.home_page import home_page_app
from webapp.application.blueprints.settings import settings_page_app
from webapp.application.blueprints.twitch_bot import twitch_bot_page_app

# For `import *`
__all__ = ['create_app']

DEFAULT_BLUEPRINTS = (
    (home_page_app, '/home'),
    (settings_page_app, '/settings'),
    (twitch_bot_page_app, '/twitch_bot')
)


def create_app(config=None, blueprints=None) -> Flask:
    """
    This function creates a Flask app instance.

    :param config:     the configuration the Flask app should be configured with
    :param blueprints: the blueprints the Flask app should register
    :returns:          Flask app
    """

    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    app = Flask(__name__)

    configure_app(app, config)
    configure_blueprints(app, blueprints)
    configure_celery(app)
    configure_error_handlers(app)
    configure_extensions(app)
    configure_root_route(app)

    return app


def configure_app(app, config=None) -> None:
    """
    This function configures the Flask app.

    :param app:    the Flask app which should be configured
    :param config: the configuration the Flask app should use; defaults to
                   ``BaseConfig`` from the ``config.py`` file if None is specified
    """

    if config:
        app.config.from_object(config)
    else:
        app.config.from_object(BaseConfig)


def configure_blueprints(app, blueprints) -> None:
    """
    This function registers the blueprints for the Flask app.

    :param app:        the Flask app which should register the blueprints
    :param blueprints: the blueprints; a tuple of tuples, with the inner tuples
                       containing the blueprint name and url_prefix
                       (see ``DEFAULT_BLUEPRINTS``)
    """

    for blueprint, url_prefix in blueprints:
        app.register_blueprint(blueprint, url_prefix=url_prefix)


def configure_celery(app) -> None:
    """
    This function:
    - updates the rest of the Celery config from the Flask config
    - creates a subclass of the task that wraps the task execution in an
      application context

    :param app: the Flask app in which celery should run
    """

    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        """Make Celery tasks work within the Flask app context."""

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask


def configure_error_handlers(app) -> None:
    """
    This function configures the error handlers for the following HTTP exceptions:
    - 404
    - 500

    :param app: the Flask app in which the HTTP errors could occur
    :returns:   the HTML error template and error code, depending on the error
                which occurred
    """

    @app.errorhandler(404)
    def page_not_found(_):
        return render_template('/http_errors/error_404.html'), 404

    @app.errorhandler(500)
    def internal_server_error(_):
        return render_template('/http_errors/error_500.html'), 500


def configure_extensions(app) -> None:
    """
    This function configures the following extensions:
    - Celery
    - Flask-SQLAlchemy
    - SocketIO

    :param app: the Flask app in which the extensions should be initialised
    """

    configure_celery(app)

    database.init_app(app)

    socketio.init_app(app, message_queue=BaseConfig.SOCKETIO_MESSAGE_QUEUE)


def configure_root_route(app) -> None:
    """
    This function redirects the user from the root of the webapp to the
    ``home_page_app`` blueprint.

    :param app: the Flask app to which the user(s) connect
    :returns:   a redirect away from the root page
    """

    @app.route('/')
    def index():
        # Call home_page function from home_page_app file
        return redirect(url_for('home_page_app.home_page'))
