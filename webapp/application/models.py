# -*- coding: utf-8 -*-
"""This file contains all the models used by the database."""

from datetime import datetime

from webapp.application.extensions import database


class TwitchBotInfoCards(database.Model):
    """
    This database model stores the information for the info cards on the
    ``twitch_bot.html`` page.

    The following columns are created:

    -  position (integer)
    -  header (string with length of 1024)
    -  data_type (string with length of 1024)
    -  data_value (string with length of 1024)
    -  html_additional_class (string with length of 1024)

    """

    id: int = database.Column(database.Integer, primary_key=True)

    position: int = database.Column(database.Integer)
    header: str = database.Column(database.String(1024))
    data_type: str = database.Column(database.String(1024))
    data_value: str = database.Column(database.String(1024))
    html_additional_class: str = database.Column(database.String(1024))


class TwitchBotConnectionOverview(database.Model):
    """
    This database model stores the information for the connection overview on
    the ``twitch_bot.html`` page.

    The following columns are created:

    -  timestamp (datetime)
    -  message (string with length of 1024)

    """

    id: int = database.Column(database.Integer, primary_key=True)

    timestamp: datetime = database.Column(database.DateTime)
    message: str = database.Column(database.String(1024))


class TwitchBotMessages(database.Model):
    """
    This database model stores the information for the (twitch) messages on
    the ``twitch_bot.html`` page.

    The following columns are created:

    -  timestamp (datetime)
    -  event (string with length of 1024)
    -  spam_flag (string with length of 1024)
    -  author (string with length of 1024)
    -  message (string with length of 1024)
    -  html_additional_class (string with length of 1024)

    """

    id: int = database.Column(database.Integer, primary_key=True)

    timestamp: datetime = database.Column(database.DateTime)
    event: str = database.Column(database.String(1024))
    spam_flag: str = database.Column(database.String(1024))
    author: str = database.Column(database.String(1024))
    message: str = database.Column(database.String(1024))
    html_additional_class: str = database.Column(database.String(1024))
