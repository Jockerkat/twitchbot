#!/usr/bin/env bash

source venv/bin/activate

# Inspiration: https://www.putorius.net/create-multiple-choice-menu-bash.html
PS3="Please choose which action you want to perform: "
actions=(
    "Run flask in production mode"
    "Run flask in development mode"
    "Run flask in development mode without reloader"
    "Reset and initialise the database"
    "quit"
)

COLUMNS=12

# Create selection menu
select action_choice in "${actions[@]}"; do
    case "$action_choice" in
        "Run flask in production mode")
            printf "\nRunning flask app in production environment ...\n"
            printf "\nPlease do not forget to start the redis server with run_redis_server.sh!\n"
            export FLASK_APP=webapp.wsgi.py FLASK_ENV=production && flask run
            break
            ;;
        "Run flask in development mode")
            printf "\nRunning flask app in development environment ...\n"
            printf "\nPlease do not forget to start the redis server with run_redis_server.sh!\n"
            export FLASK_APP=webapp.wsgi.py FLASK_ENV=development && flask run
            break
            ;;
        "Run flask in development mode without reloader")
            printf "\nRunning flask app in development environment without reloader ...\n"
            printf "\nPlease do not forget to start the redis server with run_redis_server.sh!\n"
            export FLASK_APP=webapp.wsgi.py FLASK_ENV=development && flask run --no-reload
            break
            ;;
        "Reset and initialise the database")
            printf "\nResetting the database ...\n"
            export FLASK_APP=webapp.wsgi.py FLASK_ENV=development && flask initdb
            break
            ;;
        "quit")
            printf "Quitting script."
            exit
            ;;
        *)
            printf "Invalid option, try again."
            ;;
    esac
done
