# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.0.0] - yyyy.mm.dd

## Added

- Added Dockerfile for terminal version.
- Added type hints.
- Added `run_twitch_bot.sh` to terminal version so that running TwitchBot from terminal works.
- Added function to quit gracefully when receiving the `SIGTERM` signal (important for `docker stop ...`).

## Changed

- Moved requirements into separate files (`requirements_terminal.txt`, `requirements_webapp.txt`
  and `requirements_dev.txt`).
- Split utility functions into multiple files and moved them into the root of the directory.
- Moved tests into the root of the project and improved the test cases.
- Moved documentation into the root of the project.
- Improved naming of variables, functions, etc.
- Refactored most of the code.
- Usernotice subgift now only thanks the user who gifts, not the recipients.

## Updated

- Updated all README's with an improved structure/layout.

## [3.1.1] - 2021.08.22

### Fixed

- Fixed terminal's and webapp's crashing when an `on_usernotice` event isn't handled and the `on_usernotice_data` would
  thus be empty.

## [3.1.0] - 2021.07.19

### Changed

- Changed webapp's and terminal's `on_usernotice` handling of raids, subs, resubs and subgifts to use a dictionary for
  the `event_type`, `event_data_row_class` and `messages` with one central place where the info gets sent to twitch
  chat. Makes for a much cleaner implementation.

## [3.0.3] - 2021.07.02

### Fixed

- Fixed `Flask SQLAlchemy` requirement.

## [3.0.2] - 2021.06.28

### Fixed

- Fixed `on_usernotice` timestamp not being converted to string.

### Removed

- Removed one `SPAM_LIST` entry from terminal application.

## [3.0.1] - 2021.06.27

### Fixed

- Fixed directory regex and error message in `run_*.sh`.
- Fixed #12: The `twitch_bot_config.json` is no longer initialised in the `TwitchBot`'s `__init__` function, but in
  the `on_welcome` function. See the comment there for more information.

## [3.0.0] - 2021.06.26

### Added

- Added webapp version of TwitchBot using flask for the back end.

### Changed

- Updated root README.md.
- Updated python requirements.
- Updated the terminal version so it's at the same level as the webapp.

### Removed

- Removed logging from the terminal version.

## [2.3.3] - 2021.05.07

### Changed

- Created two use scenarios for the bot: (1) running it in a terminal (implementation in the folder `terminal`) or (2)
  running it as a flask webserver (implementation will be in the folder `webserver`).

## [2.3.2] - 2021.05.07

### Removed

- Removed full stop after bot messages.

## [2.3.1] - 2021.05.03

### Fixed

- Fixed spam filter regex parsing.

## [2.3.0] - 2021.04.08

### Added

- Added ability to set the bots and the username's default chat message colour.
- Added printing and logging of raid and (re)sub events and a bot response.

### Fixed

- Fixed #2.

## [2.2.1] - 2021.04.08

### Fixed

- Fixed `on_usernotice` functions.

## [2.2.0] - 2021.04.08

### Added

- Added function to get all tags from event.
- Added function to send messages to chat.
- Added functions to react to raid and subscription.

### Changed

- Switched to cleaner way of getting tag values for `MessageReceivedTimeSent` and `MessageReceivedUsername`.
- Switched to using function to send messages to chat.

## [2.1.0] - 2021.04.08

### Fixed

- Fixed issue #1.
- Fixed issue #3.

## [2.0.1] - 2021.04.07

### Fixed

- Fixed `re.compile` crashing when encountering regex strings inside a chat message with `re.escape`.

## [2.0.0] - 2021.04.07

### Added

- Added `config.json` to set `SPAM_LIST` and `TABLE_LAYOUT`.

### Changed

- Moved functions/classes into their own `.py` files to declutter the main script.

## [1.4.2] - 2021.04.05

### Fixed

- Fixed MrDestructoid emote.

## [1.4.1] - 2021.04.05

### Added

- Added icon files.

### Changed

- Changed `@USERNAME` message to include "(I'm a bot MrDestructoid)" at the end.
- `SPAM_LIST` now contains same entries as Deckz_'s blocked terms on Twitch.

## [1.4.0] - 2021.04.05

### Added

- The bot now responds to the user (`@USERNAME`) who may have sent spam in chat.

## [1.3.0] - 2021.04.05

### Added

- Added functions to write spam check results to log file.

### Changed

- Commented out function `do_command`.

## [1.2.0] - 2021.04.05

### Added

- Added spam examples to folder Tests.

### Changed

- Checking if the message is spam not longer performs an exact match but an `re.search()`.

## [1.1.1] - 2021.04.05

### Changed

- Changed Text and Spam Flag around.
- Changed table column widths and the amount of dashes.

## [1.1.0] - 2021.04.04

### Added

- The message author is now printed as well.
- The Spam Flag and error messages are now coloured using ANSI escape sequences.

### Changed

- The function `CheckMessageForSpam` now doesn't return anything (i.e. `None`) if the chat message isn't equal to a
  string set in `SPAM_LIST` (previously it would set the variable `MessageIsSpam` to `False` and return said variable).
- Changed order and look of printed messages, added a header and nice alignment.

## [1.0.0] - 2021.04.04

- Initial release.