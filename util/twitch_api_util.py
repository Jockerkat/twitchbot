# -*- coding: utf-8 -*-
"""Utilities for the Twitch.tv API."""

import requests


def get_app_access_token(client_id: str, client_secret: str) -> dict:
    """
    This method gets an app access token from the Twitch API using the OAuth
    Client Credentials Flow procedure.

    :param client_id:     the client ID of the TwitchBot
    :param client_secret: the client secret of the TwitchBot
    :return:              JSON-encoded app access token
    """
    # The scope parameter specifies which permissions are required. A list of
    # scopes can be found here: https://dev.twitch.tv/docs/authentication/#scopes
    authorisation_parameters: dict = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'client_credentials',
        'scope': ''
    }

    return requests.post(url='https://id.twitch.tv/oauth2/token',
                         params=authorisation_parameters).json()


def get_stream_information(client_id: str, app_access_token: dict, channel: str) -> dict:
    """
    This function gets information about a specific Twitch.tv channel.
    More information here: https://dev.twitch.tv/docs/api/reference#get-streams

    :param client_id:        the client ID of the TwitchBot
    :param app_access_token: JSON-encoded app access token
    :param channel:          the Twitch.tv user login (channel) without '#'
    :return:                 JSON payload with a data field containing an array
                             of stream information elements and a pagination
                             field containing information required to query for
                             more streams
    """
    authentication_headers: dict = {
        'Client-ID': client_id,
        'Authorization': 'Bearer ' + app_access_token['access_token']
    }

    return requests.get(url='https://api.twitch.tv/helix/streams?user_login=' + channel,
                        headers=authentication_headers).json()
