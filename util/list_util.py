# -*- coding: utf-8 -*-
"""Utilities to handle lists."""


def sort_list_of_dictionaries_by_value(unsorted_list: list, dictionary_key: str) -> list:
    """
    This function sorts the dictionaries in a list by value of a dictionary key
    present in all dictionaries.

    :param unsorted_list:  the list of dictionaries, unsorted
    :param dictionary_key: the dictionary key to sort by
    :return:               the input list dictionaries sorted by value of a key
    """
    return sorted(unsorted_list, key=lambda k: k[dictionary_key])


def get_values_from_list_of_dictionaries(list_of_dictionaries: list, dictionary_key: str) -> list:
    """
    This function returns a list containing the values of a key in the list of
    dictionaries.

    :param list_of_dictionaries: a list of dictionaries
    :param dictionary_key:       the key's value to extract
    :return:                     a list containing the values of a key
    """
    return [key[dictionary_key] for key in list_of_dictionaries]
