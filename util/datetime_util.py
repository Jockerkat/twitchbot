# -*- coding: utf-8 -*-
"""Utilities to handle datetime."""

from datetime import datetime
import pytz


def posix_timestamp_to_datetime(timestamp: float, timezone: str = None) -> datetime:
    """
    This function returns the datetime, in milliseconds, from a POSIX timestamp.
    If a timezone is passed in, the timezone information is removed from the
    returned datetime.

    :param timestamp: a POSIX timestamp
    :param timezone:  the timezone if a localised datetime is needed; if
                      omitted, the returned datetime is in UTC. All available
                      timezones can be found here:
                      https://gist.github.com/heyalexej/8bf688fd67d7199be4a1682b3eec7568
    :returns:         the POSIX timestamp converted to datetime
    """
    if timezone is not None:
        return datetime.fromtimestamp(timestamp / 1000, pytz.timezone(timezone)).replace(tzinfo=None)
    else:
        return datetime.utcfromtimestamp(timestamp / 1000)
