# -*- coding: utf-8 -*-
"""Utilities for printing information in the terminal."""


class TerminalColours:
    """
    This class defines six different text colours/styles:

    -  pink/violet
    -  green
    -  yellow
    -  red
    -  white
    -  bold

    """
    HEADER: str = '\033[95m'   # pink/violet
    OK: str = '\033[92m'       # green
    WARNING: str = '\033[93m'  # yellow
    FAIL: str = '\033[91m'     # red
    END: str = '\033[0m'       # white
    BOLD: str = '\033[1m'      # bold


def print_table_header(table_layout: str) -> None:
    dash: str = '-' * 175

    print(dash)
    print(table_layout.format("Date and Time", "Event", "Spam Flag", "Author", "Text"))
    print(dash)
