# -*- coding: utf-8 -*-
"""Utilities to handle JSON files."""

import json

from typing.io import TextIO


def read_file(file: str) -> dict:
    """
    This function deserializes a JSON file and returns its contents.

    :param file: the JSON file to be read
    :return:     the JSON file's contents
    """
    with open(file, 'r') as json_file:
        return json.load(json_file)


def update_value(file: str, key: str, new_value: str) -> None:
    """
    This function updates a key's value in a JSON file.

    :param file:      the JSON file to be updated
    :param key:       the key
    :param new_value: the key's new value
    """
    with open(file, 'r+') as json_file:
        data = json.load(json_file)

        # Update value
        data[key] = new_value

        __write_to_file(json_file, data)


def update_value_in_array(file: str, key: str, array_index: int, new_value: str) -> None:
    """
    This function updates a value in an array in a JSON file.

    :param file:        the JSON file to be updated
    :param key:         the key to the array
    :param array_index: the value's index in the array
    :param new_value:   the array's index' new value
    """
    with open(file, 'r+') as json_file:
        data = json.load(json_file)

        # Update value
        data[key][array_index] = new_value

        __write_to_file(json_file, data)


def append_value_to_array(file: str, key: str, value: str) -> None:
    """
    This function appends a value to an array in a JSON file.

    :param file:  the JSON file to be updated
    :param key:   the key to the array
    :param value: the value to be appended
    """
    with open(file, 'r+') as json_file:
        data = json.load(json_file)

        # Append value
        data[key].append(value)

        __write_to_file(json_file, data)


def delete_value_in_array(file: str, key: str, array_index: int) -> None:
    """
    This function deletes a value from an array in a JSON file.

    :param file:        the JSON file to be updated
    :param key:         the key to the array
    :param array_index: the value's index in the array
    """
    with open(file, 'r+') as json_file:
        data = json.load(json_file)

        # Delete value
        del data[key][array_index]

        __write_to_file(json_file, data)


def __write_to_file(file: TextIO, data: dict) -> None:
    """
    This function writes JSON data to a JSON file by first moving the cursor
    back to the beginning of the file and then writing the contents.

    :param file: the JSON file to be written to
    :param data: the JSON data to be written
    """
    file.seek(0)
    json.dump(data, file, indent=2)
    file.truncate()
