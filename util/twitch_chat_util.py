# -*- coding: utf-8 -*-
"""Utilities related to Twitch.tv chat."""

import re

import irc.client


def send_chat_message(connection: irc.client.ServerConnection, channel: str, message: str) -> None:
    """
    This function sends a chat message in the connected IRC chat.

    :param connection: the IRC connection
    :param channel:    to which IRC channel the message should be sent
    :param message:    the message to be sent
    """
    connection.privmsg(channel, message)


def change_chat_message_colour(connection: irc.client.ServerConnection, channel: str, colour: str) -> None:
    """
    This function changes the colour of the subsequent chat message.

    :param connection: the IRC connection
    :param channel:    to which IRC channel the message should be sent
    :param colour:     the colour of the subsequent chat message
    """
    connection.privmsg(channel, f"/color {colour}")


def get_event_tags(event: irc.client.Event) -> dict:
    """
    This function returns a dictionary containing the keys and values from the
    tags of an event.

    :param event: an IRC event
    :return:      the keys and values from the tags of an event
    """
    return {tag['key']: tag['value'] for tag in event.tags}


def strip_unicode_characters(string: str) -> str:
    """
    This function strips all unicode code points (characters) from a string
    (i.e. all unicode code points above 383 (which still allows the majority of
    characters used in European languages)).

    :param string: a string
    :return:       the parameter ``string`` stripped of unicode code points
                   above 383
    """
    return "".join(char for char in string if ord(char) < 384)


def chat_message_is_spam(message: str, spam_list: list) -> bool:
    """
    This function checks whether a message should be considered spam or not.

    :param message:   the message to check
    :param spam_list: a list containing regular expressions which should be
                      considered spam
    :return:          True if message is to be considered spam, else False
    """
    _message: str = message.strip().casefold()

    for entry in spam_list:
        if re.search(re.compile(entry), _message) is not None:
            return True

    return False
