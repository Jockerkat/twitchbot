# -*- coding: utf-8 -*-
"""This file contains the entry point to the terminal application."""

import os
import sys
import signal
from dotenv import load_dotenv
from application.twitch_bot import TwitchBot
from util import json_util


def exit_gracefully(signum, frame):
    print(f"Received signal {signum}, stopping ...")
    sys.exit()


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, exit_gracefully)

    try:
        load_dotenv(dotenv_path='./application/.env')

        config: dict = json_util.read_file('./application/config.json')

        username: str = config['USERNAME']
        client_id: str = os.getenv('CLIENT_ID')
        client_secret: str = os.getenv('CLIENT_SECRET')
        token: str = os.getenv('TOKEN')
        channel: str = config['CHANNEL']

        TwitchBot(username, client_id, client_secret, token, channel).start()

    except KeyboardInterrupt:
        sys.exit("\nStopped TwitchBot terminal. Bye!")
