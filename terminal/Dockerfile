FROM python:3.8-buster

# Create new non-root user
RUN useradd --create-home --shell /bin/bash twitchbot

# Run commands from /terminal/ directory inside container
WORKDIR /home/terminal

# Copy python requirements from local to docker container
COPY ./requirements_terminal.txt ./requirements_terminal.txt

# Create python virtual environment
RUN python3 -m venv venv

# Update pip
RUN venv/bin/python3 -m pip install --upgrade pip

# Install the dependencies in the docker container
RUN venv/bin/pip3 install wheel
RUN venv/bin/pip3 install -r requirements_terminal.txt

# Copy the needed to the docker container
COPY ./terminal/application/ ./application/
COPY ./terminal/app.py ./
COPY ./util/ ./util/

# Set owner of all files and directories in /home/terminal to the new twitchbot user
RUN chown -R twitchbot:twitchbot ./

# Switch to the new twitchbot user
USER twitchbot

# Start script
ENTRYPOINT  ["venv/bin/python3", "-u", "app.py"]