# -*- coding: utf-8 -*-
"""This file contains the TwitchBot terminal version."""

import irc.bot
import irc.client
from datetime import datetime
from util import datetime_util, json_util, terminal_util, twitch_api_util, twitch_chat_util


class TwitchBot(irc.bot.SingleServerIRCBot):
    """This class connects to a Twitch.tv channel."""

    def __init__(self, username: str, client_id: str, client_secret: str, token: str, channel: str):
        self.username: str = username
        self.client_id: str = client_id
        self.client_secret: str = client_secret
        self.token: str = token
        self.channel: str = '#' + channel
        self.config: dict = json_util.read_file('./application/config.json')

        # Authorise with the Twitch API
        app_access_token: dict = twitch_api_util.get_app_access_token(self.client_id, self.client_secret)
        stream_information: dict = twitch_api_util.get_stream_information(self.client_id, app_access_token, channel)

        # Initialise IRC bot connection (connect to chat)
        server: str = 'irc.chat.twitch.tv'
        port: int = 6667

        print(f'[{datetime.utcnow()}] Connecting to {server} on port {port} ...')

        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, 'oauth:' + self.token)], username, username)

    def on_welcome(self, connection: irc.client.ServerConnection, event: irc.client.Event) -> None:
        """This function is triggered when the IRC bot connects to a Twitch.tv channel's chat."""
        print(f'[{datetime.utcnow()}]', f'Joining channel {self.channel} ...')

        # Request specific capabilities so they can be used
        connection.cap('REQ', ':twitch.tv/membership')
        connection.cap('REQ', ':twitch.tv/tags')
        connection.cap('REQ', ':twitch.tv/commands')

        connection.join(self.channel)

        print(f'[{datetime.utcnow()}]', 'Joined successfully')
        terminal_util.print_table_header(self.config['TABLE_LAYOUT'])

        # Set colour of account to default
        twitch_chat_util.change_chat_message_colour(connection, self.channel, self.config['DEFAULT_USERNAME_COLOUR'])

    def on_pubmsg(self, connection: irc.client.ServerConnection, event: irc.client.Event) -> None:
        """This function is triggered when someone sends a chat message."""
        tags: dict = twitch_chat_util.get_event_tags(event)

        timestamp: datetime = datetime_util.posix_timestamp_to_datetime(int(tags['tmi-sent-ts']))
        author: str = tags['display-name']
        message: str = event.arguments[0]

        is_spam: bool = twitch_chat_util.chat_message_is_spam(twitch_chat_util.strip_unicode_characters(message),
                                                              self.config['SPAM_LIST'])

        if is_spam:
            # Update table
            print(self.config['TABLE_LAYOUT'].format(str(timestamp),
                                                     'pubmsg',
                                                     f'{terminal_util.TerminalColours.WARNING}'
                                                     f'Potential Spam{terminal_util.TerminalColours.END}' + ' ' * 6,
                                                     author,
                                                     message))

            # Send warning in chat
            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['BOT_COLOUR_WARNING_TIMEOUT'])

            twitch_chat_util.send_chat_message(connection,
                                               self.channel,
                                               f"/me @{author}, your most recent chat "
                                               "message has been detected as potential spam "
                                               "(I'm a bot MrDestructoid )")

            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['DEFAULT_USERNAME_COLOUR'])

        else:
            print(self.config['TABLE_LAYOUT'].format(str(timestamp),
                                                     'pubmsg',
                                                     f'{terminal_util.TerminalColours.OK}'
                                                     f'Okay{terminal_util.TerminalColours.END}' + ' ' * 16,
                                                     author,
                                                     message))

    def on_usernotice(self, connection: irc.client.ServerConnection, event: irc.client.Event) -> None:
        """This function is triggered when someone raids, subscribes, etc."""
        tags: dict = twitch_chat_util.get_event_tags(event)

        event: str = tags['msg-id']
        timestamp: datetime = datetime_util.posix_timestamp_to_datetime(int(tags['tmi-sent-ts']))
        event_author: str = tags['login'] if tags['display-name'] == '' else tags['display-name']

        event_data: dict = {}

        # Update event_data for later use
        if event == 'raid':
            event_data = {'event_type': 'raid',
                          'messages': [f"/me Thank you @{event_author} for the raid! "
                                       "deckzEnergy ",
                                       f"/me Make sure to check out {event_author} at twitch.tv/{event_author} !!"]}
        elif event == 'sub':
            event_data = {'event_type': 'sub',
                          'messages': [f"/me Thank you @{event_author} for the sub! "
                                       "deckzSUBGANG "]}
        elif event == 'resub':
            event_data = {'event_type': 'resub',
                          'messages': [f"/me Thank you @{event_author} for the resub! "
                                       "deckzSUBGANG "]}
        elif event == 'submysterygift' and tags.get('msg-param-mass-gift-count') is not None:
            # Only thank the one who gifts
            if int(tags['msg-param-mass-gift-count']) == 1:
                event_data = {'event_type': 'subgift*1',
                              'messages': [f"/me Thank you @{event_author} for the gifted "
                                           "sub! deckzSUBGANG "]}
            else:
                event_data = {'event_type': f"subgift*{tags['msg-param-mass-gift-count']}",
                              'messages': [f"/me Thank you @{event_author} for the gifted "
                                           "subs! deckzSUBGANG "]}
        else:
            print(f"event {event} occurred with tags {tags}")

        # If the event isn't handled above, the dictionary `event_data` is empty, and evaluates to False
        if bool(event_data):
            # Send message(s) in chat
            twitch_chat_util.change_chat_message_colour(connection, self.channel, self.config['BOT_COLOUR_NORMAL'])

            for message in event_data['messages']:
                twitch_chat_util.send_chat_message(connection, self.channel, message)

            twitch_chat_util.change_chat_message_colour(connection,
                                                        self.channel,
                                                        self.config['DEFAULT_USERNAME_COLOUR'])

            # Update table
            print(self.config['TABLE_LAYOUT'].format(str(timestamp),
                                                     event_data['event_type'],
                                                     f'{terminal_util.TerminalColours.OK}'
                                                     f'N/A{terminal_util.TerminalColours.END}' + ' ' * 17,
                                                     event_author,
                                                     'N/A'))
