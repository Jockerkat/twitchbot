# TwitchBot - Terminal Version

## Table of Contents

- [Screenshots](#screenshots)
    - [Twitch Bot: IRC bot start](#twitch-bot-irc-bot-start)
    - [Twitch Bot: Messages received](#twitch-bot-messages-received)
    - [Twitch Bot: IRC bot stop](#twitch-bot-irc-bot-stop)
- [Quick Setup](#quick-setup)
    - [Docker](#docker)
- [Configuration](#configuration)

## Screenshots

### Twitch Bot: IRC bot start

![](screenshots/twitch_bot_started.png)

### Twitch Bot: Messages received

![](screenshots/twitch_bot_messages_received.png)
![](screenshots/twitch-tv_chat_messages.png)

### Twitch Bot: IRC bot stop

![](screenshots/twitch_bot_stopped.png)

## Quick Setup

For all steps below (except step 1), it is assumed that you are in the root directory (`/TwitchBot/`).

1. Download the latest release from [here](https://gitlab.com/Jockerkat/twitchbot/-/releases).
2. Create a python virtual environment with the following command: `python3 -m venv venv`.
3. Install the python dependencies with the following command: `pip3 install -r requirements_terminal.txt`.
4. Get your Twitch.tv *Client ID* by creating a new application on
   Twitch.tv's [developer page](https://dev.twitch.tv/console/apps). Once created, press *Manage* on the right to get
   the ID.
5. Get your Twitch.tv *Client Secret* by pressing *New Secret* at the bottom of the page from the previous step.
6. Get your Twitch.tv IRC *Token* by going to [this website](https://twitchapps.com/tmi). Click on *Connect*, then copy
   the token sans "oauth:".
7. Create a `.env` file in the `./terminal/application/` directory and add the following to it:
    ```plaintext
    CLIENT_ID = your_client_id_here
    CLIENT_SECRET = your_client_secret_here
    TOKEN = your_token_here
    ```
8. Make any modifications to the `config.json` file to suit your needs.
9. Open a terminal and start the script using the following command: `. terminal/run_twitch_bot.sh`.

In the end, your directory structure should look like this:

```bash
.
├── venv/
├── terminal/
│   ├── application/
│   │   ├── .env
│   │   ├── __init__.py
│   │   ├── config.json
│   │   └── twitch_bot.py
│   ├── documentation/
│   ├── screenshots/
│   ├── app.py
│   ├── Dockerfile
│   ├── README.md
│   └── run_twitch_bot.sh
├── tests/
├── util/
├── webapp/
├── .gitignore
├── CHANGELOG.md
├── LICENSE.md
├── README.md
├── requirements_dev.txt
├── requirements_terminal.txt
└── requirements_webapp.txt
```

### Docker

If you want to deploy the terminal version of TwitchBot with Docker, then follow these steps after completing the steps
1, 4-8 from above:

1. Build the docker image with the following command: `docker build -t twitchbot_terminal -f terminal/Dockerfile .`
2. Finally, run the docker image with this command: `docker run -d --name=twitchbot twitchbot_terminal:latest`

If you want to make changes to the `config.json` file at a later date, follow the steps in
[this](https://stackoverflow.com/a/39399158) StackOverflow answer. If you want to view the print outputs, use this
command: `docker logs twitchbot`.

## Configuration

TwitchBot can be configured with the `config.json` file:

- the `SPAM_LIST` array sets which words/phrases should be considered spam (**important**: the entries must be in lower
  case). Python's regex is fully supported.
- `USERNAME` sets the name of the TwitchBot.
- `CHANNEL` sets to which Twitch.tv channel TwitchBot should connect to.
- `BOT_COLOUR_NORMAL` sets TwitchBot's chat message colour for subscriptions, re-subscriptions, gifted subscriptions and
  raids.
- `BOT_COLOUR_WARNING_TIMEOUT` sets TwitchBot's chat message colour for warnings and timeouts.
- `DEFAULT_USERNAME_COLOUR` sets the colour of the associated Twitch.tv account (useful when using the same Twitch.tv
  account for TwitchBot and the human user).

Note that the colours must be in hex (#000000) format or one of the following values: Blue, BlueViolet, CadetBlue,
Chocolate, Coral, DodgerBlue, Firebrick, GoldenRod, Green, HotPink, OrangeRed, Red, SeaGreen, SpringGreen, YellowGreen.
