#!/usr/bin/env bash

# Add root (/TwitchBot/) to PYTHONPATH
if [[ -z $PYTHONPATH ]]; then
  PYTHONPATH=$PYTHONPATH:$(pwd)
  export PYTHONPATH
fi

# app.py will only work if called from /TwitchBot/terminal/
if ! [[ "$PWD" =~ .+?(\/terminal) ]]; then
  cd "./terminal" || exit 1
fi

python3 app.py
